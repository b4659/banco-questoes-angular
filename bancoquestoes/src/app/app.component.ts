import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './services/usuario.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private usuarioService: UsuarioService) {}

  public listaUsuarios:any = [];

  ngOnInit(): void {
    this.usuarioService.getUsuarios().subscribe((data) => {
      this.listaUsuarios = Object.values(data);
      console.log(this.listaUsuarios[0]);
    })
  }

  title = 'bancoquestoes';
}
